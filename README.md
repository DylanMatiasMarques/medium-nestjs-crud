<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[travis-image]: https://api.travis-ci.org/nestjs/nest.svg?branch=master
[travis-url]: https://travis-ci.org/nestjs/nest
[linux-image]: https://img.shields.io/travis/nestjs/nest/master.svg?label=linux
[linux-url]: https://travis-ci.org/nestjs/nest
  

## Description

Simple [NestJS](https://github.com/nestjs/nest) CRUD RESTfull API example with [@nestjsx/crud](https://github.com/nestjsx/crud)

## Installation

```bash
$ npm install
$ docker-compose up -d
```

## Running the app

```bash
$ npm run start

```

## Support

Bitcoin: 159RtLSxNfdcQvYj8rnmYvMzRtRjvMPQeH

