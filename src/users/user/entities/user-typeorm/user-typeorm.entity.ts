import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { User } from '../user.interface';
import { IsNotEmpty } from 'class-validator';

@Entity('user')
export class UserEntity implements User {
  @PrimaryGeneratedColumn() id: number;

  @IsNotEmpty()
  @Column()
  name: string;

  @IsNotEmpty()
  @Column()
  email: string;

  @IsNotEmpty()
  @Column()
  age: number;
}
