import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { UserEntity, User } from '../entities';
import { Controller } from '@nestjs/common';
import { UserService } from '../services/user.service';
import { CreateUser } from './dto/create-user.dto';

@ApiTags('User')
@Crud({
  model: {
    type: UserEntity,
  },
  routes: {
    exclude: ['createManyBase', 'replaceOneBase'],
    deleteOneBase: {
      returnDeleted: true,
    },
  },
  dto: {
    create: CreateUser,
    update: CreateUser,
  },
})
@Controller('user')
export class UserController implements CrudController<User> {
  constructor(public service: UserService) {}
}
